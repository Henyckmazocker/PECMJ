using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using UnityEngine.SceneManagement;
public class MenuManager : MonoBehaviour
{

    public float cantidad = 2;

    GameObject[] ls = new GameObject[4];

    // Start is called before the first frame update
    void Start()
    {
        
        ls = GameObject.FindGameObjectsWithTag("tank");

        ls[2].SetActive(false);
        ls[3].SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void updatecant(){

        cantidad = GameObject.Find("Slider").GetComponent<Slider>().value;

        if(cantidad == 3){
            ls[2].SetActive(true);
            ls[3].SetActive(false);
        }else{

            if(cantidad == 2){
                ls[2].SetActive(false);
                ls[3].SetActive(false);
            }else{

                ls[3].SetActive(true);
            }
        }

    }

    public void inici(){

        PlayerPrefs.SetFloat ("cantidad", cantidad);
        SceneManager.LoadScene("_Complete-Game");
    }

    public void inicicinema(){

        PlayerPrefs.SetFloat ("cantidad", cantidad);
        SceneManager.LoadScene("Cinemachine");
    }


}
